package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * Your code here. You may also write additional methods if you like.
     */
    private void start() {

        System.out.print("Please enter a range of any two numbers.\na = ");
        String a = Keyboard.readInput();

        System.out.print("b = ");
        String b = Keyboard.readInput();

        int r1 = generateRandomNuminRange(a, b);
        int r2 = generateRandomNuminRange(a, b);
        int r3 = generateRandomNuminRange(a, b);

        System.out.println("3 randomly generated numbers: " + r1 + ", " + r2 + " and " + r3);
        System.out.print("Smallest number is " + Math.min(r1,Math.min(r2,r3)));
    }

    public int generateRandomNuminRange(int lwx, int upx){

        return (int) (lwx + (upx-lwx + 1)*Math.random());
    }

    public int generateRandomNuminRange(String lowerbound, String upperbound){

        int lwx = Integer.parseInt(lowerbound);
        int upx = Integer.parseInt(upperbound);

        return generateRandomNuminRange(lwx, upx);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
